﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController Instance = null;

    public PlayerController playerController;
      
	private int IsPremium = 0;
    private int score = 0;
    private int speed = 0;
    private int level = 0;
    public int minSpeed = 5;
    public int maxSpeed = 50;
    public int minLevel = 1;
    public int maxLevel = 100;

    public float GameSpeed
    {
        get
        {
            return speed * playerController.speedMultiplier;
        }
    }

    // Game control
    public bool isGameStarted = false;
    public bool IsGameOver = false;

	// UI
	public Canvas canvas;
    public GameObject MenuPanel;
    public GameObject GameoverPanel;
    public GameObject ControlsPanel;
    public GameObject ScorePanel;
	public GameObject AdPanel;
	public GameObject RemoveAdButton;

    // Score UI
    public Text scoreText;
    public Text levelText;
    public Text bestscoreText;

    // Sound
    public AudioSource coinSound;

    private void Awake()
    {
        Instance = this;
		Application.targetFrameRate = 60;
    }

    private void Start()
    {
        playerController = FindObjectOfType<PlayerController>();

        ScorePanel.SetActive(false);
        GameoverPanel.SetActive(false);
        ControlsPanel.SetActive(false);
        MenuPanel.SetActive(true);

		IsMute = PlayerPrefs.GetInt("IsMute", 0) == 0 ? false : true;
		soundOn.enabled = !IsMute;
		soundOff.enabled = IsMute;

		AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
		foreach (AudioSource aSource in audioSources)
			aSource.enabled = !IsMute;

		IsPremium = PlayerPrefs.GetInt("IsPremium", 0);
    }

    public void StartGame()
    {
        MenuPanel.SetActive(false);
        ControlsPanel.SetActive(true);
        ScorePanel.SetActive(true);

        score = 0;
        level = minLevel;
        speed = minSpeed;
        scoreText.text = "Score\n" + score.ToString();
        levelText.text = "Level\n" + level.ToString();

        isGameStarted = true;
        playerController.StartRunning();
        ObstacleSpawner.Instance.SpawnObstacle();

		RectTransform rt = AdPanel.GetComponent<RectTransform>();

		if(Screen.dpi <= 400f)
		{
			rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 32f);
		}
		else if(Screen.dpi > 400f && Screen.dpi <= 720f)
		{
			rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 50f);
		}
		else if(Screen.dpi > 720f)
		{
			rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 90f);
		}

		if (IsPremium == 0)
		{
			AdPanel.SetActive(true);
			AdMobController.Instance.RequestBanner();
		}
    }

    public void GameOver()
    {
        CancelInvoke("SpawnObstacle");
        IsGameOver = true;
        ControlsPanel.SetActive(false);
        GameoverPanel.SetActive(true);
        int bestscore = PlayerPrefs.GetInt("BestScore", 0);
        if (score > bestscore)
        {
            bestscore = score;
            PlayerPrefs.SetInt("BestScore", bestscore);
        }
        bestscoreText.text = "Best\n" + bestscore.ToString();

		GPGController.Instance.SubmitScore(score);

		RemoveAdButton.SetActive(IsPremium == 0 ? true : false);
    }

	public Image soundOn;
	public Image soundOff;
	bool IsMute = false;
	public void OnSound()
	{
		IsMute = !IsMute;
		print("IsMute: " + IsMute);
		soundOn.enabled = !IsMute;
		soundOff.enabled = IsMute;

		AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
		foreach (AudioSource aSource in audioSources)
			aSource.enabled = !IsMute;

		PlayerPrefs.SetInt("IsMute", (IsMute ? 1 : 0));
	}

	public void OnLeaderboard()
	{
		GPGController.Instance.OpenLeaderboard();
	}

	public void OnRestartGame()
    {
		if (IsPremium == 0)
		{
			AdPanel.SetActive(false);
			AdMobController.Instance.RemoveBanner();
			UnityAdsController.Instance.ShowInterstitial();
		}

        SceneManager.LoadScene(0);
    }

	public void OnRemoveAds()
	{
		PlayerPrefs.SetInt("IsPremium", 1);
		PlayerPrefs.Save();
		IsPremium = 1;

		// Remove Banner
		AdMobController.Instance.RemoveBanner();
		// Remove Adpanel
		AdPanel.SetActive(false);
		// Remove ad button
		RemoveAdButton.SetActive(false);
	}

	public void OnRating()
	{
		#if UNITY_ANDROID
		Application.OpenURL("market://details?id=com.hoshimi.f1hit");
		#elif UNITY_IPHONE
		Application.OpenURL("itms-apps://itunes.apple.com/app/com.hoshimi.f1hit");
		#endif
	}

    int lastScoreLane = 0;
    int maxScoreAllowedInOneLane = 2;
    int laneScoreCount = 0;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Obstacle")
        {
            coinSound.Play();
            score++;
            if (score % 5 == 0)
            {
                level++;
                speed += 2;
                speed = Mathf.Clamp(speed, minSpeed, maxSpeed);
            }

            // Check if player is scoring on the same lane, if so force the next obstacle to spawn on that lane to make it more difficult
            if (playerController.currentLane == lastScoreLane)
            {
                laneScoreCount++;
                if (laneScoreCount > maxScoreAllowedInOneLane)
                {
                    ObstacleSpawner.Instance.forcedLane = true;
                    laneScoreCount = 0;
                }
            }
            else
                lastScoreLane = playerController.currentLane;

            float levelDelta = (float)level / (float)maxLevel;
            ObstacleSpawner.Instance.spawnDistance = Mathf.Lerp(10f, 4f, levelDelta);
            // Play some sound

            scoreText.text = "Score\n" + score.ToString();
            levelText.text = "Level\n" + level.ToString();

			switch(level)
			{
				case 2:
					GPGController.Instance.UnlockAchievement(GPGSIds.achievement_learner);
					break;
				case 4:
					GPGController.Instance.UnlockAchievement(GPGSIds.achievement_agent47);
					break;
				case 6:
					GPGController.Instance.UnlockAchievement(GPGSIds.achievement_le_petoulet);
					break;
				case 8:
					GPGController.Instance.UnlockAchievement(GPGSIds.achievement_newtown);
					break;
				case 10:
					GPGController.Instance.UnlockAchievement(GPGSIds.achievement_original_flying_finn);
					break;
				case 12:
					GPGController.Instance.UnlockAchievement(GPGSIds.achievement_big_al);
					break;
				case 14:
					GPGController.Instance.UnlockAchievement(GPGSIds.achievement_fletcher);
					break;
				case 16:
					GPGController.Instance.UnlockAchievement(GPGSIds.achievement_king_of_the_beach);
					break;
				case 18:
					GPGController.Instance.UnlockAchievement(GPGSIds.achievement_emmo);
					break;
				case 20:
					GPGController.Instance.UnlockAchievement(GPGSIds.achievement_king_of_f2);
					break;
			}
        }
    }
}
