﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingObject : MonoBehaviour {

    private Rigidbody2D rb2d;
    private float speed = 1.5f;
    // Use this for initialization
    private void Start()
    {
        //Get and store a reference to the Rigidbody2D attached to this GameObject.
        rb2d = GetComponent<Rigidbody2D>();

    }

    private void FixedUpdate()
    {
        // If the game is over, stop scrolling.
        if (!GameController.Instance.isGameStarted || GameController.Instance.IsGameOver)
        {
            return;// if(rb2d) rb2d.velocity = Vector2.zero;
        }
        else
        {
            Vector3 moveVector = new Vector3(0f, -speed, 0f);
            Vector2 finalPosition = moveVector * GameController.Instance.GameSpeed * Time.fixedDeltaTime;
            rb2d.MovePosition(rb2d.position + finalPosition);
        }
    }
}
