﻿using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    public static ObstacleSpawner Instance = null;

    private float spawnY = 12f;
    public float spawnDistance = 10f;
    public GameObject obstaclePrefab;
    private List<ObstacleController> obstacles = new List<ObstacleController>();
    private ObstacleController lastObstacle;
    private int lastLane = 0;

    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        if (!GameController.Instance.isGameStarted || GameController.Instance.IsGameOver || lastObstacle == null)
            return;
        
        if(spawnY - lastObstacle.transform.position.y >= spawnDistance)
        {
            SpawnObstacle();
        }
    }
    public bool forcedLane = false;
    public void SpawnObstacle()
    {
        lastObstacle = obstacles.Find(x => !x.gameObject.activeSelf);
        if (lastObstacle == null)
        {
            GameObject go = Instantiate(obstaclePrefab, transform);
            lastObstacle = go.GetComponent<ObstacleController>();
            obstacles.Add(lastObstacle);
        }
         
        float lanePossibility = Random.Range(0f, 100f);
        int desiredLane = 0;
        if (lanePossibility < 33)
            desiredLane = -1;
        else if (lanePossibility > 66)
            desiredLane = 1;
        
        if (forcedLane)
        {
            desiredLane = GameController.Instance.playerController.currentLane;
            forcedLane = false;
        }

        lastObstacle.transform.position = new Vector3(desiredLane * 2, spawnY, 0f);
        lastObstacle.Spawn();
    }
}
