﻿using GooglePlayGames;
using UnityEngine;

public class GPGController : MonoBehaviour {

	public static GPGController Instance = null;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			DestroyImmediate(gameObject);
		}
	}

	private void Start()
	{
		InitializeGPGS();
	}

	private void InitializeGPGS()
	{
		PlayGamesPlatform.Activate();

		if (!Social.localUser.authenticated)
		{
			Social.localUser.Authenticate((bool success) =>
			{
				OnConnectionResponse(success);
			});
		}
	}

	public void SubmitScore(int score)
	{
		Social.ReportScore(score, GPGSIds.leaderboard_f1_points_table, (bool success) => {
			Debug.Log("Reported score to leaderboard " + success.ToString());
		});
	}

	public void UnlockAchievement(string achievementID)
	{
		Social.ReportProgress(achievementID, 100.0f, (bool success) => {
			Debug.Log("Achievement Unloacked " + success.ToString());
		});
	}

	public void OpenLeaderboard()
	{
		if (Social.localUser.authenticated)
		{
			Social.ShowLeaderboardUI();
		}
		else
		{
			Social.localUser.Authenticate((bool success) =>
			{
				OnConnectionResponse(success);
			});
		}
	}

	void OnConnectionResponse(bool authenticated)
	{
		if (authenticated)
		{
			// Show success
			Debug.Log("Authenticated Success!");
		}
		else
		{
			// Show failure
			Debug.Log("Authenticated Failure!");
		}
	}
}
