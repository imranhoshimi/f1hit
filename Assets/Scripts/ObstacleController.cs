﻿using UnityEngine;

public class ObstacleController : MonoBehaviour
{
	public Sprite[] sprites;
	public SpriteRenderer visual;
    public GameObject explosion;

    private bool isRunning = false;
    private Rigidbody2D rb2d;
    private float speed = 0.75f;

    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();    
    }

    public void Spawn()
    {
        gameObject.SetActive(true);
		visual.sprite = sprites[Random.Range(0, sprites.Length - 1)];
		visual.enabled = true;
        explosion.SetActive(false);
        isRunning = true;
    }

    public void DeSpawn()
    {
        isRunning = false;
        gameObject.SetActive(false);
    }
    public void Crashed()
    {
        isRunning = false;
        // Play crash animation
		visual.enabled = false;
        explosion.SetActive(true);
    }

    private void Update()
    {
        if (!isRunning || GameController.Instance.IsGameOver)
            return;

        if(transform.position.y <= -5)
        {
            DeSpawn();            
        }        
    }

    private void FixedUpdate()
    {
        if (!isRunning || GameController.Instance.IsGameOver)
            return;

        Vector3 moveVector = new Vector3(0f, -speed, 0f);
        Vector2 finalPosition = moveVector * GameController.Instance.GameSpeed * Time.fixedDeltaTime;
        rb2d.MovePosition(rb2d.position + finalPosition);
    }
}
