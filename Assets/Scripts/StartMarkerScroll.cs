﻿using UnityEngine;

public class StartMarkerScroll : MonoBehaviour {
    private float speed = 1.5f;
    private void FixedUpdate()
    {
        if (!GameController.Instance.isGameStarted || GameController.Instance.IsGameOver)
            return;

        Vector3 forceVector = new Vector3(0f, -speed, 0f);

        // Move obstacle on Z axis (towards player)
        transform.position += forceVector * GameController.Instance.GameSpeed * Time.fixedDeltaTime;
        if (transform.position.y <= -6f)
            gameObject.SetActive(false);
    }
}
