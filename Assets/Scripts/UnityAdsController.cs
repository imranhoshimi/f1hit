﻿using UnityEngine.Advertisements;
using UnityEngine;

public class UnityAdsController : MonoBehaviour {

	public static UnityAdsController Instance = null;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			DestroyImmediate(gameObject);
		}
	}

	private void Start()
	{
#if UNITY_ANDROID
		string appId = "2666544";
#elif UNITY_IPHONE
		string appId = "2666545";
#else
        string appId = "unexpected_platform";
#endif
		Advertisement.Initialize(appId);
	}

	public void ShowInterstitial()
	{
		Advertisement.Show();
	}
}
