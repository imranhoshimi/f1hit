﻿using GoogleMobileAds.Api;
using UnityEngine;

public class AdMobController : MonoBehaviour {

	public static AdMobController Instance = null;

	private BannerView bannerView;
	private InterstitialAd interstitial;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			DestroyImmediate(gameObject);
		}
	}

	private void Start()
	{
		InitializeAdmob();
	}

	private void InitializeAdmob()
	{
#if UNITY_ANDROID
		string appId = "ca-app-pub-2822831632126025~9780959784";
#elif UNITY_IPHONE
        string appId = "not ready";
#else
        string appId = "unexpected_platform";
#endif

		MobileAds.SetiOSAppPauseOnBackground(true);

		// Initialize the Google Mobile Ads SDK.
		MobileAds.Initialize(appId);
	}

	// Returns an ad request with custom ad targeting.
	private AdRequest CreateAdRequest()
	{
		return new AdRequest.Builder()
			                //.AddTestDevice(AdRequest.TestDeviceSimulator)
			                //.AddTestDevice("B896CEEB985702873D3008506C292961")
			                //.AddKeyword("game")
							//.SetGender(Gender.Male)
							//.SetBirthday(new DateTime(1985, 1, 1))
							//.TagForChildDirectedTreatment(false)
							//.AddExtra("color_bg", "9B30FF")
							.Build();
	}

	public void RequestBanner()
	{
#if UNITY_EDITOR
		string adUnitId = "unused";
#elif UNITY_ANDROID
		string adUnitId = "ca-app-pub-2822831632126025/2521750007";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/2934735716";	// test ad
#else
        string adUnitId = "unexpected_platform";
#endif

		// Clean up banner ad before creating a new one.
		if (bannerView != null)
		{
			this.bannerView.Destroy();
		}

		this.bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Top);

		// Called when an ad request has successfully loaded.
		this.bannerView.OnAdLoaded += HandleOnBannerAdLoaded;

		// Load the banner with the request.
		this.bannerView.LoadAd(this.CreateAdRequest());
	}

	public void RemoveBanner()
	{
		if (this.bannerView != null)
			this.bannerView.Destroy();
	}

	public void RemoveInterstitial()
	{
		if (this.interstitial != null)
			this.interstitial.Destroy();
	}

	void HandleOnBannerAdLoaded(object sender, System.EventArgs e)
	{
		//RectTransform rt = GameController.Instance.AdPanel.GetComponent<RectTransform>();
		//float scaleFactor = GameController.Instance.canvas.scaleFactor;
		//rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, bannerView.GetHeightInPixels());

		GameController.Instance.AdPanel.SetActive(false);
	}

	public void RequestInterstitial()
	{
		// These ad units are configured to always serve test ads.
#if UNITY_EDITOR
		string adUnitId = "unused";
#elif UNITY_ANDROID
		string adUnitId = "ca-app-pub-2822831632126025/7092793679";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/4411468910";	// test ad
#else
        string adUnitId = "unexpected_platform";
#endif

		// Clean up interstitial ad before creating a new one.
		if (this.interstitial != null)
		{
			this.interstitial.Destroy();
		}

		// Create an interstitial.
		this.interstitial = new InterstitialAd(adUnitId);

		// Load an interstitial ad.
		this.interstitial.LoadAd(this.CreateAdRequest());
	}
	
	public void ShowInterstitial()
	{
		if (this.interstitial.IsLoaded())
		{
			this.interstitial.Show();
		}
		else
		{
			Debug.Log("Interstitial is not ready yet");
		}
	}
}
