﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private bool isRunning = false;

    // Movement
    public  int currentLane = 0;
    public float speedMultiplier = 1f;

    public GameObject visual;
    public GameObject explosion;

    public AudioSource crashSound;
    public AudioSource carSound;
    public AudioSource moveSound;
    public AudioSource startSound;

    private void Start()
    {
        carSound.pitch = 0.75f;
        visual.SetActive(true);
        explosion.SetActive(false);
        carSound.Stop();
    }

    public void StartRunning()
    {
        isRunning = true;
        startSound.Play();
        carSound.PlayDelayed(0.1f);

    }

    private void Crashed()
    {
        isRunning = false;
        // Play crash animation
        carSound.Stop();
        crashSound.Play();
        visual.SetActive(false);
        explosion.SetActive(true);

        GameController.Instance.GameOver();
    }

    public void OnRight()
    {
        if (GameController.Instance.IsGameOver)
            return;

        if (currentLane < 1)
            currentLane++;
        
        transform.position = new Vector3(2 * currentLane, transform.position.y, transform.position.z);

        moveSound.Play();
    }

    public void OnLeft()
    {
        if (GameController.Instance.IsGameOver)
            return;

        if (currentLane > -1)
            currentLane--;

        transform.position = new Vector3(2 * currentLane, transform.position.y, transform.position.z);

        moveSound.Play();
    }

    public void OnAccelerate()
    {
        if (GameController.Instance.IsGameOver)
            return;

        // increase gamespeed
        speedMultiplier = 1f;
        carSound.pitch = 1f;
    }

    public void OnDeccelerate()
    {
        if (GameController.Instance.IsGameOver)
            return;

        // decrease gamespeed
        speedMultiplier = 0.5f;
        carSound.pitch = 0.75f;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Obstacle")
        {
            collision.GetComponent<ObstacleController>().Crashed();
            print("Crashed");
            Crashed();
        }
    }
}
